const express = require('express');
const router = express.Router();
const autoController = require('./controllers/autoController');
const propietarioController = require('./controllers/propietarioController');
const servicioController = require('./controllers/servicioController');
const { body  } = require('express-validator');

// Crear un servicio
router.post('/servicios/', [
    body('nombre','El nombre es obligatorio.').not().isEmpty(),
    body('precio','El precio es obligatorio.').not().isEmpty(),
    body('precio','El precio es debe ser un valor decimal.').isDecimal(),
  ],
  servicioController.crear
);

// Listar servicios
router.get('/servicios/', servicioController.listar);

// Crear un servicio
router.put('/servicios/:id', [
    body('nombre','El nombre es obligatorio.').not().isEmpty(),
    body('precio','El precio es obligatorio.').not().isEmpty(),
    body('precio','El precio es debe ser un valor decimal.').isDecimal(),
  ],
  servicioController.modificar
);


// Crear un propietario
router.post('/propietarios/', [
    body('apellido','El apellido es obligatorio.').not().isEmpty(),
    body('nombre','El nombre es obligatorio.').not().isEmpty(),
    body('documento','El documento es obligatorio.').not().isEmpty(),
  ],
  propietarioController.crear
);

// Listar propietarios
router.get('/propietarios/', propietarioController.listar);

// Crear un auto de un propietario
router.post('/propietarios/:id/autos', [
    body('marca','La marca es obligatoria.').not().isEmpty(),
    body('modelo','El modelo es obligatorio.').not().isEmpty(),
    body('anio','El año es obligatorio.').not().isEmpty(),
    body('patente','La patente es obligatoria').not().isEmpty(),
    body('color','El color es obligatorio.').not().isEmpty()
  ],
  propietarioController.crearAuto
);

// Crear un auto
router.post('/autos/', [
    body('marca','La marca es obligatoria.').not().isEmpty(),
    body('modelo','El modelo es obligatorio.').not().isEmpty(),
    body('anio','El año es obligatorio.').not().isEmpty(),
    body('patente','La patente es obligatoria').not().isEmpty(),
    body('color','El color es obligatorio.').not().isEmpty(),
    body('propietario','El propietario es obligatorio.').isMongoId()
  ],
  autoController.crear
);

// Listar autos
router.get('/autos/', autoController.listar);

// Mostrar un autos
router.get('/autos/:id', autoController.show);

// Mostrar historial de un autos
router.get('/autos/:id/historial', autoController.historial);

// Borrar un auto
router.delete('/autos/:id', autoController.borrar);

// Modificar un auto
router.put('/autos/:id',
  [
    body('marca','La marca es obligatoria.').not().isEmpty(),
    body('modelo','El modelo es obligatorio.').not().isEmpty(),
    body('anio','El año es obligatorio.').not().isEmpty(),
    body('patente','La patente es obligatoria').not().isEmpty(),
    body('color','El color es obligatorio.').not().isEmpty()
  ],
  autoController.editar
);

// Cargar servicios a un auto
router.post('/autos/:id/servicios', [
    body('servicios', 'Los servicios son obligatorios. Debe cargar al menos uno.').isArray({ min: 1 }),
    body("servicios.*",'Los servicios no són válidos.').not().isArray().isMongoId()
  ],
  autoController.cargarServicios
);

module.exports = router;
