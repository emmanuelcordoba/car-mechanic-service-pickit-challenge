const database = require('./database');
const dotenv = require('dotenv');
dotenv.config();
const Servicio = require('./models/Servicio');

database.connect();

const seedServicios = [
  {
    nombre: 'Cambio de Aceite',
    precio: 120
  },
  {
    nombre: 'Cambio de Filtro',
    precio: 150
  },
  {
    nombre: 'Cambio de Correa',
    precio: 130
  },
  {
    nombre: 'Revisión General',
    precio: 300
  },
  {
    nombre: 'Pintura',
    precio: 200
  },
  {
    nombre: 'Otro',
    precio: 100
  }
];

const seedDB = async () => {
  await Servicio.deleteMany({});
  await Servicio.insertMany(seedServicios);
  console.log('Seeder ejecutado correctamente');
};

seedDB().then(() => {
  database.close();
})

