const express = require("express");
const dotenv = require('dotenv');
dotenv.config();
const database = require('./database');
const morgan = require('morgan');
const cors = require('cors');

const APP = express();
const PORT = process.env.PORT || 4000;

database.connect();

APP.use(cors());
APP.use(morgan('dev'));

APP.use(express.json());
APP.use(express.urlencoded({ extended: false }));

APP.use('/', require('./routes'));

APP.listen(PORT, () =>{
  console.log(`Server running at localhost:${PORT}`);
});
