const mongoose = require('mongoose');
const Servicio = require('../models/Servicio');
const { validationResult } = require('express-validator');

exports.crear = async (req, res) => {

    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(412).json({ errors: errores.array() });
    }

    try {
        let servicio = new Servicio(req.body);

        await servicio.save();
        return res.status(201).json({ msg: 'Servicio creado correctamente.', servicio });
    } catch (error) {
        console.error(error);
        res.status(400).json({ msg: 'Hubo un error.' })
    }
};

exports.listar = async (req, res) => {
  try {
      const servicios = await Servicio.find().sort('servicio');
      res.json({ servicios });
  } catch (error) {
      console.error(error);
      res.status(400).json({ msg: 'Hubo un error.' })
  }
};

exports.modificar = async (req, res) => {

  const errores = validationResult(req);
  if(!errores.isEmpty()){
      return res.status(412).json({ errors: errores.array() });
  }

  try {

    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
      return res.status(404).json({ msg: 'El servicio no existe.' });
    }

      // Varificar que el servicio exista
      let servicio = await Servicio.findById(req.params.id);

      if(!servicio){
        return res.status(404).json({ msg: 'El servicio no existe.' });
      }

      servicio = await Servicio.findByIdAndUpdate(req.params.id, req.body, { new: true})

      res.json({ msg: 'Servicio modificado correctamente.', servicio });

  } catch (error) {
      console.error(error);
      res.status(400).json({ msg: 'Hubo un error.' })
  }
};
