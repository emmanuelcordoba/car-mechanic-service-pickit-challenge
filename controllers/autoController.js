const mongoose = require('mongoose');
const Auto = require('../models/Auto');
const Propietario = require('../models/Propietario');
const { validationResult } = require('express-validator');
const Servicio = require('../models/Servicio');

exports.crear = async (req, res) => {

    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(412).json({ errors: errores.array() });
    }

    try {

      const propietario = await Propietario.findById(req.body.propietario);
      if(!propietario){
          return res.status(412).json({ msg: 'El propietario no existe.' })
      }

      let auto = new Auto(req.body);
      await auto.save();

      return res.status(201).json({ msg: 'Auto creado correctamente.', auto });

    } catch (error) {
        console.error(error);
        return res.status(400).json({ msg: 'Hubo un error.' });
    }
};

exports.listar = async (req, res) => {

  try {

      const autos = await Auto.find().sort('nombre').populate('propietario');
      return res.json({ autos });

  } catch (error) {
      console.error(error);
      return res.status(400).json({ msg: 'Hubo un error.' });
  }
};

exports.borrar = async (req,res) => {
  try {
    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Varificar que el auto exista
    let auto = await Auto.findById(req.params.id);

    if(!auto){
      return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Eliminar auto
    await auto.remove();
    return res.json({ msg: 'Auto eliminado correctamente.' });

  } catch (error) {
    console.error(error);
    return res.status(400).json({ msg: 'Hubo un error.' });
  }
};

exports.show = async (req,res) => {
  try {
    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Varificar que el auto exista
    let auto = await Auto.findById(req.params.id).populate('propietario');

    if(!auto){
      return res.status(404).json({ msg: 'El auto no existe.' });
    }

    return res.json({ auto });

  } catch (error) {
    console.error(error);
    return res.status(400).json({ msg: 'Hubo un error.' });
  }
};

exports.historial = async (req,res) => {
  try {
    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Varificar que el auto exista
    let auto = await Auto.findById(req.params.id);

    if(!auto){
      return res.status(404).json({ msg: 'El auto no existe.' });
    }

    return res.json({ historial: auto.historial });

  } catch (error) {
    console.error(error);
    return res.status(400).json({ msg: 'Hubo un error.' });
  }
};

exports.editar = async (req, res) => {

  // Revisar si hay errores
  const errores = validationResult(req);
  if(!errores.isEmpty()){
      return res.status(412).json({ errors: errores.array() });
  }

  try {
    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
        return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Varificar que el auto exista
    let auto = await Auto.findById(req.params.id);

    if(!auto){
        return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Modificar auto
    auto = await Auto.findByIdAndUpdate(req.params.id, req.body, { new: true})

    return res.json({ msg: 'Auto modificado correctamente.', auto });

} catch (error) {
  console.error(error);
  return res.status(400).json({ msg: 'Hubo un error.' });
  }
};

exports.cargarServicios = async (req, res) => {
  // Revisar si hay errores
  const errores = validationResult(req);
  if(!errores.isEmpty()){
      return res.status(412).json({ errors: errores.array() });
  }

  try {
    // Validar el ID
    if(!mongoose.Types.ObjectId.isValid(req.params.id)){
      return res.status(404).json({ msg: 'El auto no existe.' });
    }

    // Varificar que el auto exista
    let auto = await Auto.findById(req.params.id);

    if(!auto){
      return res.status(404).json({ msg: 'El auto no existe.' });
    }

    let servicios = [];
    let total = 0;
    for (let i = 0; i < req.body.servicios.length; i++) {
      let servicio = await Servicio.findById(req.body.servicios[i]);
      if(!servicio)
      {
        return res.status(412).json({ msg: 'Alguno de los servicios no es válido.' });
      }
      total += servicio.precio;

      if(auto.color.toUpperCase() === 'GRIS' && servicio.servicio === 'Pintura')
      {
        return res.status(412).json({ msg: 'El servicio de Pintura no esta disponible para automóviles de color gris.' });
      }

      servicios.push(servicio);
    }

    auto.historial.push({servicios, total});
    await auto.save();

    return res.json({ auto });

} catch (error) {
    console.error(error);
    return res.status(400).json({ msg: 'Hubo un error.' });
  }
}
